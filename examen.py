from autor import Autor
from libro import Libro

def get_list(nombre_fichero):
    f = open(nombre_fichero, mode="rt", encoding="utf-8")
    linea = f.readline()
    dicc = {}
    if linea != "":
        while linea != "" :
            palabras = linea.split()
            for palabra in palabras:
                res = dicc.get(len(palabra))
                if res is not None:
                    listado = dicc.get(len(palabra))
                    if listado.count(palabra) == 0:
                        # Te explique que se que no añadix a la llista pero no entenc perque, no li vec llogica. Es soles esta linea, el resto crec que está be
                        dicc[len(palabra)] = listado.append(palabra)
                else:
                    dicc[len(palabra)] = [palabra]
            linea = f.readline()
        f.close()
        return dicc
    else:
        raise ValueError("La lista no puede estar vacia")

def mas_antiguos(lista, fecha):
    if fecha > 1900 and fecha < 2021:
        lista_def = []
        for libro in lista:
            if Libro.get_anyo(libro) <= fecha:
                lista_def.append(Libro.get_titulo(libro))
        return lista_def
    else:
        raise ValueError("El año  no es valido")

        

l1 = Libro(Autor(59,"Juan","Sobrino"),"Las rosas",2012)
l2 = Libro(Autor(259,"Pepe","Nose"),"Casa",1998)
l3 = Libro(Autor(5,"Carlos","Manuel"),"Ordenador",2000)
l4 = Libro(Autor(589,"Sara","Estar"),"Tiendas",1500)

listaLibro = [l1, l2, l3, l4]

#print(mas_antiguos(listaLibro,2000))

print(get_list("fichero.txt"))
