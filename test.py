import unittest
from libro import Libro
from autor import Autor
import examen

class Pruebas(unittest.TestCase):
    def test_lista_definitiva_tam(self):
        l1 = Libro(Autor(59,"Juan","Sobrino"),"Las rosas",2012)
        l2 = Libro(Autor(259,"Pepe","Nose"),"Casa",1998)
        l3 = Libro(Autor(5,"Carlos","Manuel"),"Ordenador",2000)
        l4 = Libro(Autor(589,"Sara","Estar"),"Tiendas",1500)

        listaLibro = [l1, l2, l3, l4]
        l1 = examen.mas_antiguos(listaLibro,2010)
        l2 = ["Casa","Ordenador","Tiendas"]
        self.assertEqual(len(l1),len(l2))

    def test_error_fecha(self):
        l1 = Libro(Autor(59,"Juan","Sobrino"),"Las rosas",2012)
        l2 = Libro(Autor(259,"Pepe","Nose"),"Casa",1998)
        l3 = Libro(Autor(5,"Carlos","Manuel"),"Ordenador",2000)
        l4 = Libro(Autor(589,"Sara","Estar"),"Tiendas",1500)

        listaLibro = [l1, l2, l3, l4]
        with self.assertRaises(ValueError) as cm:
            l1 = examen.mas_antiguos(listaLibro,1500)
        self.assertEqual(str(cm.exception), "El año  no es valido")

    def test_nombres_libros(self):
        l1 = Libro(Autor(59,"Juan","Sobrino"),"Las rosas",2012)
        l2 = Libro(Autor(259,"Pepe","Nose"),"Casa",1998)
        l3 = Libro(Autor(5,"Carlos","Manuel"),"Ordenador",2000)
        l4 = Libro(Autor(589,"Sara","Estar"),"Tiendas",1500)

        listaLibro = [l1, l2, l3, l4]
        l1 = examen.mas_antiguos(listaLibro,2010)
        l2 = ["Casa","Ordenador","Tiendas"]
        self.assertEqual(l1,l2)

class Suite(unittest.TestSuite):
    def __init__(self):
        super(Suite, self).__init__()
        self.addTest(Pruebas('test_lista_definitiva_tam'))
        self.addTest(Pruebas('test_error_fecha'))
        self.addTest(Pruebas('test_nombres_libros'))

if __name__ == "__main__":    
    runner = unittest.TextTestRunner()
    my_suite = Suite()
    runner.run(my_suite)

