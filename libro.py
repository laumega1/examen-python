from autor import Autor

class Libro():
    def __init__(self, aut, tit, any):
        self.__autor = aut
        self.__titulo = tit
        self.__anyo = any

    def get_anyo(self):
        return self.__anyo

    def get_titulo(self):
        return self.__titulo
